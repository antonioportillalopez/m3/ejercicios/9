<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Vehiculo;
use clases\Camion;
use clases\Autobus;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $camion=new Camion();
        $camion->encender();
        $camion->cargar(10);
        $camion->verficar_encendido();
        $camion->matricula='MDU-293';
        $camion->apagar();
        $autobus= new Autobus();
        $autobus->encender();
        $autobus->subir_pasajeros(5);
        $autobus->verficar_encendido();
        $autobus->matricula='KDF-923';
        $autobus->apagar();
        
        
        ?>
    </body>
</html>
