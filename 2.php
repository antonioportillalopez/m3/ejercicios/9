<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Persona;

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $padre= new Persona('Ramón', 'Abramo', '35');
        $hijo=&$padre; // se crea un objeto referenciado a donde esta la memoria el otro -- esto en php es equivalente $hijo=$padre
        $hija= clone $padre;
        $hijo->setEdad(100);
        $hija->setEdad(50);
        echo 'Objeto hijo ';var_dump($hijo);
        echo 'Objeto padre ';var_dump($padre);
        echo 'Objeto hija ';var_dump($hija);
        ?>
    </body>
</html>
