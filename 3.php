<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Person;

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $ingles= new Person('Peter');
        echo 'mi nombre es: '.$ingles->decirNombre().'<br/>';
        $ingles->adquirirNombre('Pedro');
        echo 'Ahora mi nombre es '. $ingles->decirNombre().'<br/>';
               
        $ingles->hablar('La maquina del café está fuera de servicio');
        
        $ingles->hablar('El servicio técnico no me hace caso');
        
        
        ?>
    </body>
</html>
