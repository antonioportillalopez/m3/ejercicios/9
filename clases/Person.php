<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace clases;

/**
 * Description of Person
 *
 * @author antpo
 */
class Person {
    private $nombre;
    private $dormido;
    private $frases;
            
    function __construct($nombre,$frase='') {
        $this->adquirirNombre($nombre);
        $this->frases=$frase
        ;
    }
                
    public function adquirirNombre($name){
        $this->nombre=$name;
        return $this;
    }
    
    public function decirNombre(){
        return $this->nombre;
    }
    
    public function dormir(){
      $this->dormido=true;
      echo 'Mi estado ahora de dormido es: '. $this->dormido;
    }
    
    public function despertar(){
      $this->dormido=false;
      echo 'Mi estado ahora de dormido es: '. $this->dormido;
    }
    
    public function hablar ($decir){
        $this->frases= $this->frases. '-'.$decir.'<br />';
        echo 'Esto es lo que he dicho: <br/>'. $this->contar();
    }
    
    public function contar(){
        return $this->frases;
    }
    
    
}
